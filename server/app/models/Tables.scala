package models
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.driver.MySQLDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.driver.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Practicepage.schema ++ Test.schema ++ User.schema
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Practicepage
   *  @param id Database column Id SqlType(INT), AutoInc, PrimaryKey
   *  @param userid Database column UserId SqlType(INT)
   *  @param pagename Database column PageName SqlType(VARCHAR), Length(100,true)
   *  @param structureinfo Database column StructureInfo SqlType(VARCHAR), Length(30000,true), Default(None)
   *  @param deleted Database column Deleted SqlType(CHAR), Default(F) */
  case class PracticepageRow(id: Int, userid: Int, pagename: String, structureinfo: Option[String] = None, deleted: Char = 'F')
  /** GetResult implicit for fetching PracticepageRow objects using plain SQL queries */
  implicit def GetResultPracticepageRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]], e3: GR[Char]): GR[PracticepageRow] = GR{
    prs => import prs._
    PracticepageRow.tupled((<<[Int], <<[Int], <<[String], <<?[String], <<[Char]))
  }
  /** Table description of table PracticePage. Objects of this class serve as prototypes for rows in queries. */
  class Practicepage(_tableTag: Tag) extends Table[PracticepageRow](_tableTag, "PracticePage") {
    def * = (id, userid, pagename, structureinfo, deleted) <> (PracticepageRow.tupled, PracticepageRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(userid), Rep.Some(pagename), structureinfo, Rep.Some(deleted)).shaped.<>({r=>import r._; _1.map(_=> PracticepageRow.tupled((_1.get, _2.get, _3.get, _4, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column Id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("Id", O.AutoInc, O.PrimaryKey)
    /** Database column UserId SqlType(INT) */
    val userid: Rep[Int] = column[Int]("UserId")
    /** Database column PageName SqlType(VARCHAR), Length(100,true) */
    val pagename: Rep[String] = column[String]("PageName", O.Length(100,varying=true))
    /** Database column StructureInfo SqlType(VARCHAR), Length(30000,true), Default(None) */
    val structureinfo: Rep[Option[String]] = column[Option[String]]("StructureInfo", O.Length(30000,varying=true), O.Default(None))
    /** Database column Deleted SqlType(CHAR), Default(F) */
    val deleted: Rep[Char] = column[Char]("Deleted", O.Default('F'))
  }
  /** Collection-like TableQuery object for table Practicepage */
  lazy val Practicepage = new TableQuery(tag => new Practicepage(tag))

  /** Entity class storing rows of table Test
   *  @param id Database column Id SqlType(INT), AutoInc, PrimaryKey
   *  @param userid Database column UserId SqlType(INT)
   *  @param info Database column Info SqlType(VARCHAR), Length(30000,true), Default(None)
   *  @param completed Database column Completed SqlType(CHAR), Default(F)
   *  @param deleted Database column Deleted SqlType(CHAR), Default(F) */
  case class TestRow(id: Int, userid: Int, info: Option[String] = None, completed: Char = 'F', deleted: Char = 'F')
  /** GetResult implicit for fetching TestRow objects using plain SQL queries */
  implicit def GetResultTestRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[Char]): GR[TestRow] = GR{
    prs => import prs._
    TestRow.tupled((<<[Int], <<[Int], <<?[String], <<[Char], <<[Char]))
  }
  /** Table description of table Test. Objects of this class serve as prototypes for rows in queries. */
  class Test(_tableTag: Tag) extends Table[TestRow](_tableTag, "Test") {
    def * = (id, userid, info, completed, deleted) <> (TestRow.tupled, TestRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(userid), info, Rep.Some(completed), Rep.Some(deleted)).shaped.<>({r=>import r._; _1.map(_=> TestRow.tupled((_1.get, _2.get, _3, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column Id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("Id", O.AutoInc, O.PrimaryKey)
    /** Database column UserId SqlType(INT) */
    val userid: Rep[Int] = column[Int]("UserId")
    /** Database column Info SqlType(VARCHAR), Length(30000,true), Default(None) */
    val info: Rep[Option[String]] = column[Option[String]]("Info", O.Length(30000,varying=true), O.Default(None))
    /** Database column Completed SqlType(CHAR), Default(F) */
    val completed: Rep[Char] = column[Char]("Completed", O.Default('F'))
    /** Database column Deleted SqlType(CHAR), Default(F) */
    val deleted: Rep[Char] = column[Char]("Deleted", O.Default('F'))
  }
  /** Collection-like TableQuery object for table Test */
  lazy val Test = new TableQuery(tag => new Test(tag))

  /** Entity class storing rows of table User
   *  @param id Database column Id SqlType(INT), AutoInc, PrimaryKey
   *  @param username Database column UserName SqlType(VARCHAR), Length(100,true)
   *  @param password Database column Password SqlType(VARCHAR), Length(100,true)
   *  @param deleted Database column Deleted SqlType(CHAR), Default(F) */
  case class UserRow(id: Int, username: String, password: String, deleted: Char = 'F')
  /** GetResult implicit for fetching UserRow objects using plain SQL queries */
  implicit def GetResultUserRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Char]): GR[UserRow] = GR{
    prs => import prs._
    UserRow.tupled((<<[Int], <<[String], <<[String], <<[Char]))
  }
  /** Table description of table User. Objects of this class serve as prototypes for rows in queries. */
  class User(_tableTag: Tag) extends Table[UserRow](_tableTag, "User") {
    def * = (id, username, password, deleted) <> (UserRow.tupled, UserRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(username), Rep.Some(password), Rep.Some(deleted)).shaped.<>({r=>import r._; _1.map(_=> UserRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column Id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("Id", O.AutoInc, O.PrimaryKey)
    /** Database column UserName SqlType(VARCHAR), Length(100,true) */
    val username: Rep[String] = column[String]("UserName", O.Length(100,varying=true))
    /** Database column Password SqlType(VARCHAR), Length(100,true) */
    val password: Rep[String] = column[String]("Password", O.Length(100,varying=true))
    /** Database column Deleted SqlType(CHAR), Default(F) */
    val deleted: Rep[Char] = column[Char]("Deleted", O.Default('F'))
  }
  /** Collection-like TableQuery object for table User */
  lazy val User = new TableQuery(tag => new User(tag))
}
