package controllers

import javax.inject._
import play.api._
import play.api.mvc._

@Singleton
class PracticeController @Inject() extends Controller {
  
  def index() = Action {
    Ok(views.html.developerTest())
  }
//  
}