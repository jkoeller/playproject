package app

import scala.scalajs.js.annotation.JSExportTopLevel
import scala.scalajs.js.timers.setInterval

import org.scalajs.dom
import org.scalajs.dom
import org.scalajs.dom.Event
import org.scalajs.dom.html
import org.scalajs.jquery.jQuery

import structures.DataStructure
import structures.DataStructure
import structures.node.BSTNode
import structures.node.DLLNode
import structures.node.Node
import structures.node.SLLNode

class TestCanvasManager(val structure: DataStructure, val refStructure: DataStructure) {

  structure.push(0)
  refStructure.push(0)

  private val canvas: html.Canvas = dom.document.getElementById("canv").asInstanceOf[html.Canvas]
  private val gc = canvas.getContext("2d").asInstanceOf[dom.CanvasRenderingContext2D]
  private var selected: Option[structure.NodeType] = None
  private var md = false
  private var showResult = false
  private var pf:Char = 'F'
  private val nodes = collection.mutable.Buffer[structure.NodeType]()
  private val step: Int = 5

  canvas.onmousemove = (f: dom.MouseEvent) => {
    if (selected.isDefined && md == true) {
      val sz = Node.size / 2
      selected.get.hardUpdate(f.pageX - shiftx, f.pageY - shifty)
    }
  }
  canvas.onmousedown = (e: dom.MouseEvent) => {
    md = true
    if (nodes.head.contains(e.pageX - shiftx, e.pageY - shifty) && nodes.length > 0) {
      selected = Some(nodes.head)
    }
  }

  canvas.onmouseup = (e: dom.MouseEvent) => {
    md = false
    if (selected.isDefined) {
      var min = 1e10
      var index = 0
      for (i <- structure.babies.size-1 to 0 by -1) {
        val dist = selected.get.distance(structure.babies(i))
        if (dist < min) {
          min = dist
          index = i
        }
      }
      structure.push(selected.get.data, index, selected.get.getX.toInt, selected.get.getY.toInt, selected.get.getX.toInt - structure.children(index).getX.toInt)
      refStructure.push(selected.get.data)
    }
    nodes.trimStart(1)
    nodes.head.hardUpdate(50, 50)
    selected = None
  }

  def shiftx: Double = canvas.offsetLeft
  def shifty: Double = canvas.offsetTop

  def setNodes(n: Seq[structure.NodeType]) = {
    nodes.clear()
    nodes.appendAll(n)
  }

  def generateNodes(z: Int = 15) = {
    val min = 0
    var n = z + 1
    val max = 2 * n;
    var tmpnodes = List[Int]()
    for (i <- 0 to n) {
      var tmp:Int = 0
      while (tmpnodes.contains(tmp) || tmp == 0) {
    	  tmp = (Math.floor(Math.random() * (max - min + 1)) + min).toInt
      }
      tmpnodes ::= tmp
    }
    structure.printInfo(0) match {
      case '1' => {
        val nod = for (i <- 0 until tmpnodes.length) yield {
          new SLLNode(-800, -800, tmpnodes(i)).asInstanceOf[structure.NodeType];
        }
        setNodes(nod)

      }
      case '3' => {
        val nod = for (i <- 0 until tmpnodes.length) yield {
          new DLLNode(-800, -800, tmpnodes(i)).asInstanceOf[structure.NodeType];
        }
        setNodes(nod)

      }
      case '0' => {
        val nod = for (i <- 0 until tmpnodes.length) yield {
          new BSTNode(-800, -800, tmpnodes(i)).asInstanceOf[structure.NodeType];
        }
        setNodes(nod)
      }
    }
    nodes(0).hardUpdate(50, 50)
  }

  def start() = {
    generateNodes(15)
    setInterval(50) { drawStructure() }
    def drawStructure(): Unit = {
      if (structure != null) {
        gc.clearRect(0, 0, canvas.width, canvas.height)
        for (i <- nodes) i.refresh(gc, step)
        structure.refresh(gc, step)
        if (refStructure != null && showResult) refStructure.refresh(gc, step)
      }

    }
  }

  def retry() = {
    structure.clear()
    refStructure.clear()
    structure.push(0)
    refStructure.push(0)
    generateNodes(15)
    showResult = false
  }

  def submit() = {
    if (structure.equals(refStructure)) {
      jQuery("#msgP").html("Correct!");
      pf = 'P'
    } else {
      pf = 'F'
      jQuery("#msgP").html("Incorrect Answer. Please view solution or try again.");
    }
    jQuery("#popup").show();
  }

  def showSolution() = {
    showResult = true
  }

  def save() = {
    jQuery.get("/saveTest/"+descriptionString()+"/"+pf)

  }

  def descriptionString(): String = {
    "Building a " + structure.describe

  }

}

object TestCanvasManager {
  def apply(ds: DataStructure, rds: DataStructure)(nodes: Seq[ds.NodeType]): TestCanvasManager = {
    val ret = new TestCanvasManager(ds, rds)
    val node2 = nodes.map(i => i.asInstanceOf[ret.structure.NodeType])
    ret.setNodes(node2)
    ret

  }
}
