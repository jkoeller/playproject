package app

import structures.ordering.QueueOrdering
import structures.ordering.StackOrdering
import structures.DataStructure
import structures.BST
import structures.DLL
import structures.ordering.PriorityQueueOrdering
import structures.ordering.Order
import structures.SLL

object Parsing {
  def stringToStructure(str: String): (DataStructure, Int, String) = {
    var map = {};
    val ordering = str(1);
    val kind = str(0);
    val id = str.slice(3, 4);
    val nodeStr = str.slice(str.indexOf(':') + 1, str.indexOf(';'));
    val nodeStrs = nodeStr.split(',');
    val pgName = str.slice(str.indexOf(';') + 1, str.length());
    var nodes = List[Int]()
    for (i <- nodeStrs) nodes ::= i.toInt
    val struct = parseStructure(kind.toString(), ordering.toString())._1
    struct.insertArray(nodes)
    (struct, id.toInt, pgName)
  }

  def structureToString(structure: DataStructure, pgid: Int, pgName: String): String = {
    structure.printInfo + ":" + pgid + structure.printChildren + ";" + pgName;
  }

  def parseStructure(name: String, order: String): (DataStructure, DataStructure) = {
    var struc: DataStructure = null;
    var refstruc: DataStructure = null;
    if (name == "0") {
      struc = new BST(300, 0);
      refstruc = new BST(600, 0);
    } else {
      var ordering: Order = null;
      if (order == "1") {
        ordering = new PriorityQueueOrdering();
      } else if (order == "2") {
        ordering = new QueueOrdering();
      } else {
        ordering = new StackOrdering();
      }
      if (name == "1") {
        struc = new SLL(400, 200, ordering);
        refstruc = new SLL(400, 600, ordering);
      } else {
        struc = new DLL(400, 200, ordering);
        refstruc = new DLL(400, 600, ordering);

      }
    }
    (struc, refstruc);
  }

}