package app

import scala.scalajs.js
import scala.scalajs.js.JSApp
import scala.scalajs.js.annotation.JSExportTopLevel
import scala.scalajs.js.timers.setInterval

import org.scalajs.dom
import org.scalajs.dom.document
import org.scalajs.dom.html
import org.scalajs.dom.raw.Element
import org.scalajs.jquery.jQuery
import structures.node.Node
import structures.node.SLLNode
import structures.DataStructure
import structures.Drawable
import structures.DLL
import structures.SLL
import structures.ordering.Order
import structures.ordering.StackOrdering
import structures.ordering.QueueOrdering

import org.scalajs.dom.html.Canvas
import org.scalajs.jquery.jQuery
import org.scalajs.dom.raw.HTMLElement
import scala.scalajs.js.annotation.JSExportTopLevel
import structures.BST
import structures.ordering.PriorityQueueOrdering
import structures.ordering.BSTOrdering

class TestHandler {
  var runner: Option[TestCanvasManager] = None
  var ordering: Order = null
  var listType: Int = 0

  def create(): Unit = {
    var bst = false
    var DS: DataStructure = null
    var refDS: DataStructure = null
    if (ordering != null && listType != 0) {
      listType match {
        case 1 => {
          DS = new SLL(400, 200, ordering)
          refDS = new SLL(400, 500, ordering)
          bst = false
        }
        case 2 => {
          DS = new DLL(400, 200, ordering)
          refDS = new DLL(400, 500, ordering)
          bst = false
        }
        case 3 => {
          DS = new BST(200, 200)
          refDS = new BST(600, 200)
          bst = true
          println("Made a bst")
        }
      }
      listType = 0
    }
    restoreDefaults()
    runner = Some(new TestCanvasManager(DS, refDS))
    println(runner.get.structure.describe)
    runner.get.start()
  }

  def restoreDefaults(): Unit = {
    jQuery("#structurebutton").text("Structure")
    jQuery("#listbutton").text("List")
  }

  def loadTest(str: String) = {
    val ret = Parsing.stringToStructure(str)
    val ret2 = Parsing.stringToStructure(str)
    runner = Some(new TestCanvasManager(ret._1, ret2._1))
    runner.get.start()
  }
  def start(): Unit = {
    println("practiceHandler initializied")
    jQuery("#stack").click(() => {
      jQuery("#structurebutton").text(jQuery("#stack").text)
      ordering = new StackOrdering()
    })

    jQuery("#queue").click(() => {
      jQuery("#structurebutton").text(jQuery("#queue").text)
      ordering = new QueueOrdering()
    })
    jQuery("#pq").click(() => {
      jQuery("#structurebutton").text(jQuery("#pq").text)
      ordering = new PriorityQueueOrdering()
    })
    jQuery("#bst").click(() => {
      jQuery("#structurebutton").text(jQuery("#bst").text)
      listType = 3
      ordering = new BSTOrdering()
    })
    jQuery("#slinked").click(() => {
      jQuery("#listbutton").text(jQuery("#slinked").text)
      if (listType != 3) listType = 1
    })
    jQuery("#dlinked").click(() => {
      jQuery("#listbutton").text(jQuery("#dlinked").text)
      if (listType != 3) listType = 2
    })
    jQuery("#createbutton").click(() => {
      create()
    })
    jQuery("#submitButton").click(() => {
      if (runner.isDefined) runner.get.submit()
    })
    jQuery("#retryBtn").click(() => {
      if (runner.isDefined) runner.get.retry()
    })
    jQuery("#solnBtn").click(() => {
      if (runner.isDefined) runner.get.showSolution()
    })
    jQuery("#saveBtn").click(() => {
      if (runner.isDefined) runner.get.save()
      runner = None
    })
  }

}