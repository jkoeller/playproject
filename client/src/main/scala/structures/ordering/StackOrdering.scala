package structures.ordering

import structures.node.Node

class StackOrdering extends Order {
  override def insert[T <: Node](elem: Double, array: collection.mutable.Buffer[T], hardIndex: Int = -1): Int = {
    if (hardIndex == -1) 0 else hardIndex
  }
  override def remove[T <: Node](arr: collection.mutable.Buffer[T]): Int = {
    0
  }

  override def print: String = "3"
  override def describe: String = "Stack Ordered "

}