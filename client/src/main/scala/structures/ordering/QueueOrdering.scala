package structures.ordering

import structures.node.Node

class QueueOrdering extends Order {
  override def insert[T <: Node](elem: Double, array: collection.mutable.Buffer[T], hardIndex: Int = -1): Int = {
    if (hardIndex != -1) hardIndex else 0
  }
  override def remove[T <: Node](arr: collection.mutable.Buffer[T]): Int = {
    arr.length - 1
  }

  override def print: String = "2"
  override def describe: String = "Queue Ordered "

}