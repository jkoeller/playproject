package structures.ordering

import structures.node.Node

class PriorityQueueOrdering extends Order {
  override def insert[T <: Node](elem: Double, array: collection.mutable.Buffer[T], hardIndex: Int = -1): Int = {
    if (hardIndex != -1) hardIndex else {
        var rover = 0
        while (rover<array.length && array(rover).data < elem) rover += 1 
        rover
    }
  }
  override def remove[T <: Node](arr:collection.mutable.Buffer[T]):Int = {
    0
  }
  
  override def print:String = "1"
  
  override def describe:String = "Priority-Queue Ordered "
}