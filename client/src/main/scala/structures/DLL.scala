package structures

import structures.ordering.Order
import org.scalajs.dom

import structures.node.Node
import structures.node.DLLNode
import structures.node.CentNode

class DLL(override var x: Double, override var y: Double, ordering: Order) extends DataStructure {

  type NodeType = DLLNode
  val cent = new CentNode(400, y, 0)
  override def refresh(gc: dom.CanvasRenderingContext2D, step: Int): Unit = {
    super.refresh(gc, step)
    cent.refresh(gc, step)
  }

  def push(data: Double, index: Int = -1, x: Int = 400, y: Int = 0, diff: Int = 0): Unit = {
    val ind = ordering.insert(data, children, index)
    val node = new DLLNode(x, y, data)
    println("index = " + ind + " of " + size())
    if (diff > 0) children.insert(ind+1,node) else children.insert(ind,node)
    movePointers
    updateChildrenPositions
  }

  def pop(): Unit = {
    val index = ordering.remove(children)
    children(index).prv.setNext(children(index).nxt)
    children(index).nxt.setPrev(children(index).prv)
    children.remove(index)
    movePointers
    updateChildrenPositions();

  }

  //	override def babies:List[NodeType] = super.babies

  override def insertArray(elems: Seq[Int]): Unit = {
    for (i <- elems) push(i)
  }

  override def insertStatic(index: Int, elem: Int): Unit = {
    val node = new DLLNode(400, y, elem)
    children.insert(index, node)
    movePointers
    updateChildrenPositions
  }

  override def size(): Int = children.length

  override def remove(elem: Int): Unit = {

  }

  private def movePointers {
    for (i <- 0 until children.length - 1) {
      children(i).setNext(children(i + 1))
    }
    for (i <- 1 until children.length) {
      children(i).setPrev(children(i - 1))
    }
    children(0).setPrev(cent)
    children(children.length - 1).setNext(cent)
    cent.setNext(children(0))
    cent.setPrev(children(children.length - 1))
  }

  private def updateChildrenPositions(): Unit = {
    for (c <- 0 until this.children.length) {
      val midpt = this.children.length / 2
      val newX = (this.x + (c - midpt) * Node.size * 2) + (Node.size * 2 / (midpt % 2 + 1))
      this.children(c).updatePos(newX, this.y)
    }
    cent.updatePos(x, y - 100)
  }

  override def printInfo: String = {
    "3" + this.ordering.print;
  }
//  override def printChildren: String = {
//    var str = ""
//    for (i <- children) {
//      str = str + i.data + ","
//    }
//    return str.slice(0, str.length - 1);
//  }

  override def describe: String = ordering.describe + "Double Linked List"

}