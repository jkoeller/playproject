package structures.node

import org.scalajs.dom
import structures.Drawable

trait Node extends Drawable {
  var data: Double //Marco changed to var because I am assigning to data on BST
  protected var x: Double
  protected var y: Double
  protected var fx: Double
  protected var fy: Double
  protected val pointers = collection.mutable.Buffer[Pointer]()

  def updatePos(xx: Double, yy: Double) = {
    fx = xx
    fy = yy
  }

  def hardUpdate(xx: Double, yy: Double) = {
    fx = xx
    fy = yy
    x = xx
    y = yy
  }

  override def refresh(gc: dom.CanvasRenderingContext2D, step: Int): Unit = {
    if (x != fx) {
      if (x > fx) {
        x -= Math.min(step, x - fx)
      } else {
        x += Math.min(step, fx - x)
      }
    }
    if (y != fy) {
      if (y > fx) {
        y -= Math.min(step, y - fy)
      } else {
        y += Math.min(step, fy - y)
      }
    }
    for (i <- pointers) i.refresh(gc, step)
  }

  def draw(gc: dom.CanvasRenderingContext2D): Unit = {
    gc.fillStyle = "black";
    val sz = Node.size / 2
    gc.rect(x - sz, y + sz, Node.size, Node.size);
    gc.stroke();
    gc.fillText(data.toString(), x, y + 30);
    for (i <- pointers) i.draw(gc)
  }

  def distance(other: Node): Double = {
    val dx = other.x - x
    val dy = other.y - y
    Math.sqrt(dx * dx + dy * dy)
  }

  def contains(x: Double, y: Double): Boolean = {
    val sz = Node.size / 2;

    return x + sz >= this.x && x + sz <= this.x + Node.size && y - sz >= this.y && y - sz <= this.y + Node.size;
  }
  def getX: Double = x
  def getY: Double = y
}

object Node {
  val size = 50
}