package structures.node

import org.scalajs.dom
import org.scalajs.dom.html
import dom.document
//import scala.scalajs.js.annotation.JSExportTopLevel
//import org.scalajs.jquery.jQuery

class DLLNode(override protected var x: Double, override protected var y: Double, override var data: Double) extends Node {
  override protected var fx = x
  override protected var fy = y
  var nxt: DLLNode = this
  var prv: DLLNode = this
  addPointer()
  addPointer()

  def addPointer(): Unit = {
    pointers += new Pointer(this.getRightAnchor._1,
      this.getRightAnchor._2, this.getRightAnchor._1, this.getRightAnchor._2)
  }
  def setNext(n: DLLNode) = {
    nxt = n
  }

  def setPrev(n: DLLNode) = {
    prv = n
  }

  def getRightAnchor: (Double, Double) = {
    (x + Node.size / 2, y + Node.size)
  }

  def getLeftAnchor: (Double, Double) = {
    //TODO
    (x - Node.size / 2, y + Node.size)
  }

  override def refresh(gc: dom.CanvasRenderingContext2D, step: Int): Unit = {
    pointers(0).updateOrigin(getLeftAnchor._1, getLeftAnchor._2 + 5)
    pointers(0).updateDest(prv.getRightAnchor._1, prv.getRightAnchor._2 + 5)
    pointers(1).updateOrigin(getRightAnchor._1, getRightAnchor._2 - 5)
    pointers(1).updateDest(nxt.getLeftAnchor._1, nxt.getLeftAnchor._2 - 5)
    super.refresh(gc, step)
    draw(gc)
  }

  override def draw(gc: dom.CanvasRenderingContext2D): Unit = {
    gc.fillStyle = "black";
    val sz = Node.size / 2
    gc.beginPath()
    gc.moveTo(x - sz, y + sz)
    gc.lineTo(x - sz, y + sz + Node.size)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x - sz, y + sz)
    gc.lineTo(x - sz + Node.size, y + sz)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x - sz + Node.size, y + sz + Node.size)
    gc.lineTo(x - sz + Node.size, y + sz)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x - sz + Node.size, y + sz + Node.size)
    gc.lineTo(x - sz, y + sz + Node.size)
    gc.stroke()
    gc.fillText(data.toString(), x, y + sz + 30);
    for (i <- pointers) i.draw(gc)
  }

}