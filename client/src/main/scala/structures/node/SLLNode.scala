package structures.node

import org.scalajs.dom
import org.scalajs.dom.html
import dom.document

class SLLNode (override var x:Double, override var y:Double, var data:Double) extends Node {
  override protected var fx = x
  override protected var fy = y
  private var next:SLLNode = this
  def updateNext(node:SLLNode):Unit = {
  	this.next = node
    if(pointers.isEmpty) {
      this.addPointer()
    }
    //this.pointers(0).updateOrigin(this.getRightAnchor._1, this.getRightAnchor._2)
    //this.pointers(0).updateDest(this.next.getLeftAnchor._1,this. next.getLeftAnchor._2)
  }
  def getRightAnchor:(Double,Double) = {
    (x + Node.size/2,y+Node.size)
  }
  def getLeftAnchor:(Double,Double) = {
    (x-Node.size/2,y+Node.size)
  }
  def deletePointer():Unit = {
    pointers.clear()
  }
  def addPointer():Unit = {
    pointers += new Pointer(this.getRightAnchor._1,
    this.getRightAnchor._2, this.getRightAnchor._1,this.getRightAnchor._2)
  }
  def getNext():SLLNode = {
    next
  }
  def ps = pointers.length
  override def refresh(gc:dom.CanvasRenderingContext2D,step:Int):Unit = {
    if(pointers.length>0) {
      pointers(0).updateOrigin(getRightAnchor._1, getRightAnchor._2)
      pointers(0).updateDest(this.next.getLeftAnchor._1, this.next.getLeftAnchor._2)
    }
    super.refresh(gc, step)
    draw(gc)
  }
  override def draw(gc: dom.CanvasRenderingContext2D): Unit = {
    gc.fillStyle = "black";
    val sz = Node.size / 2
    gc.beginPath()
    gc.moveTo(x-sz, y+sz)
    gc.lineTo(x-sz, y+sz+Node.size)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x-sz, y+sz)
    gc.lineTo(x-sz+Node.size, y+sz)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x-sz+Node.size, y+sz+Node.size)
    gc.lineTo(x-sz+Node.size, y+sz)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x-sz+Node.size, y+sz+Node.size)
    gc.lineTo(x-sz, y+sz+Node.size)
    gc.stroke()
    gc.fillText(data.toString(), x, y+sz + 30);
    for (i <- pointers) i.draw(gc)
  }
}