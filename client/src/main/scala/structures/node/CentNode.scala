package structures.node

import org.scalajs.dom
import org.scalajs.dom.html
import dom.document
//import scala.scalajs.js.annotation.JSExportTopLevel
//import org.scalajs.jquery.jQuery
class CentNode(xx: Double, yy: Double, ddata: Int) extends DLLNode(xx, yy, ddata) {

  override def getRightAnchor: (Double, Double) = {
    super.getLeftAnchor
  }

  override def getLeftAnchor: (Double, Double) = {
    //TODO
    super.getRightAnchor
  }
  override def refresh(gc: dom.CanvasRenderingContext2D, step: Int): Unit = {
    super.refresh(gc, step)
    pointers(0).updateOrigin(getRightAnchor._1, getRightAnchor._2 + 5)
    pointers(0).updateDest(prv.getLeftAnchor._1, prv.getLeftAnchor._2 + 5)
    pointers(1).updateOrigin(getLeftAnchor._1, getLeftAnchor._2 - 5)
    pointers(1).updateDest(nxt.getRightAnchor._1, nxt.getRightAnchor._2 - 5)
    draw(gc)
  }

}